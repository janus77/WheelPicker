package com.janus.wheelpicker;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.flowerfat.wheellib.expend.io.OnPopupListener;
import com.flowerfat.wheellib.expend.view.YearPopup;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "【wheel】";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onWheelClick();
            }
        });

    }

    private void onWheelClick() {
        YearPopup popupWheel = new YearPopup(this, 1970, 2017);
        popupWheel.setInitPosition(2);
        popupWheel.setWheelLoop(false).setNoticeText("请选择年份");
        popupWheel.show(new OnPopupListener() {
            @Override
            public void ok(String clickStr) {
                Log.e(TAG, "clickStr:" + clickStr);
            }

            @Override
            public void cancel() {
                Log.e(TAG, "cancel...");
            }
        });
        int color = Color.parseColor("#ff0000");
        popupWheel.getCancelTv().setTextColor(color);
        popupWheel.getOkTv().setTextColor(color);
    }

}
