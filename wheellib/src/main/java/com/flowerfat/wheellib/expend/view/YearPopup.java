package com.flowerfat.wheellib.expend.view;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by janus on 2017/7/6.
 * YearPopup
 */

public class YearPopup extends PopupWheel {
    public YearPopup(Context context, List<String> wheelList) {
        super(context, wheelList);
    }

    public YearPopup(Context context, int beginYear, int endYear) {
        super(context, null);
        if (beginYear < endYear) {
            ArrayList list = new ArrayList();

            for (int i = beginYear; i < endYear; ++i) {
                list.add(i + "年");
            }

            this.setWheelList(list);
        }
    }
}
