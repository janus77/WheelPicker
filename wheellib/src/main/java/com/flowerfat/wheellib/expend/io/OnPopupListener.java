package com.flowerfat.wheellib.expend.io;

/**
 * Created by janus on 2017/7/6.
 * OnPopupListener
 */

public interface OnPopupListener {
    void ok(String clickStr);

    void cancel();
}
