package com.flowerfat.wheellib.expend.view;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flowerfat.wheellib.LoopView;
import com.flowerfat.wheellib.OnItemSelectedListener;
import com.flowerfat.wheellib.R;
import com.flowerfat.wheellib.expend.io.OnPopupListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by janus on 2017/7/6.
 * PopupWheel
 */

public class PopupWheel {
    private OnPopupListener mListener;
    public View layoutView;
    private Context context;
    private Dialog dialog;
    private LoopView loopView;
    List<String> wheelList = new ArrayList();
    private String selectItemStr;

    public PopupWheel(Context context, List<String> wheelList) {
        this.context = context;
        this.wheelList = wheelList;
        builder();
    }

    private void initViews() {
        initLoopView();
        initClickView();
    }

    private void initClickView() {
        layoutView.findViewById(R.id.popYear_cancelTv).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mListener.cancel();
                dialog.dismiss();
            }
        });
        layoutView.findViewById(R.id.popYear_okTv).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mListener.ok(selectItemStr);
                dialog.dismiss();
            }
        });
    }

    private void initLoopView() {
        loopView = (LoopView) layoutView.findViewById(R.id.popYear_loopview);
        loopView.setNotLoop();
        loopView.setListener(new OnItemSelectedListener() {
            public void onItemSelected(int index) {
                Log.d("debug", "Item " + index);
                selectItemStr = wheelList.get(index);
            }
        });
        loopView.setItems(wheelList);
        loopView.setTextSize(20.0F);
    }

    private PopupWheel builder() {
        if (layoutView != null) {
            return this;
        } else {
            layoutView = LayoutInflater.from(context).inflate(R.layout.pop_year, null);
            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();
            layoutView.setMinimumWidth(display.getWidth());
            initViews();
            dialog = new Dialog(context, R.style.PopupWindowDialogStyle);
            dialog.setContentView(layoutView);
            Window dialogWindow = dialog.getWindow();
            dialogWindow.setGravity(83);
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.x = display.getWidth();
            lp.y = 0;
            dialogWindow.setAttributes(lp);
            return this;
        }
    }

    public void show() {
        dialog.show();
    }

    public void show(OnPopupListener listener) {
        setOnPopupListener(listener);
        dialog.show();
    }

    public PopupWheel setNoticeText(String text) {
        if (layoutView == null) {
            builder();
        }

        View child = ((LinearLayout) layoutView).getChildAt(0);
        TextView noticeTv = (TextView) ((LinearLayout) child).getChildAt(1);
        noticeTv.setText(text);
        return this;
    }

    public TextView getCancelTv() {
        if (layoutView == null) {
            throw new NullPointerException("请先builder");
        } else {
            View child = ((LinearLayout) layoutView).getChildAt(0);
            return (TextView) ((LinearLayout) child).getChildAt(0);
        }
    }

    public TextView getNoticeTv() {
        if (layoutView == null) {
            throw new NullPointerException("请先builder");
        } else {
            View child = ((LinearLayout) layoutView).getChildAt(0);
            return (TextView) ((LinearLayout) child).getChildAt(1);
        }
    }

    public TextView getOkTv() {
        if (layoutView == null) {
            throw new NullPointerException("请先builder");
        } else {
            View child = ((LinearLayout) layoutView).getChildAt(0);
            return (TextView) ((LinearLayout) child).getChildAt(2);
        }
    }

    public PopupWheel setWheelLoop(boolean isLoop) {
        if (isLoop) {
            loopView.setLoop();
        } else {
            loopView.setNotLoop();
        }

        return this;
    }

    public PopupWheel setInitPosition(int position) {
        int realPosition = position;
        if (position < 0) {
            realPosition = 0;
        } else if (position > wheelList.size()) {
            realPosition = wheelList.size();
        }

        loopView.setInitPosition(realPosition);
        selectItemStr = wheelList.get(realPosition);
        return this;
    }

    public PopupWheel setWheelList(List<String> list) {
        wheelList = new ArrayList();
        wheelList.addAll(list);
        loopView.setItems(wheelList);
        return this;
    }

    public void setOnPopupListener(OnPopupListener listener) {
        this.mListener = listener;
    }
}
