package com.flowerfat.wheellib;

import java.util.TimerTask;

import static com.flowerfat.wheellib.MessageHandler.WHAT_INVALIDATE_LOOP_VIEW;
import static com.flowerfat.wheellib.MessageHandler.WHAT_ITEM_SELECTED;

/**
 * Created by janus on 2017/7/6.
 * SmoothScrollTimerTask
 */

final class SmoothScrollTimerTask extends TimerTask {
    int realTotalOffset;
    int realOffset;
    int offset;
    final LoopView loopView;

    SmoothScrollTimerTask(LoopView loopview, int offset) {
        this.loopView = loopview;
        this.offset = offset;
        this.realTotalOffset = 2147483647;
        this.realOffset = 0;
    }

    @Override
    public final void run() {
        if (realTotalOffset == 2147483647) {
            realTotalOffset = offset;
        }

        realOffset = (int) ((float) realTotalOffset * 0.1F);
        if (realOffset == 0) {
            if (realTotalOffset < 0) {
                realOffset = -1;
            } else {
                realOffset = 1;
            }
        }

        if (Math.abs(realTotalOffset) <= 0) {
            loopView.cancelFuture();
            loopView.handler.sendEmptyMessage(WHAT_ITEM_SELECTED);
        } else {
            loopView.totalScrollY += realOffset;
            loopView.handler.sendEmptyMessage(WHAT_INVALIDATE_LOOP_VIEW);
            realTotalOffset -= realOffset;
        }

    }
}
