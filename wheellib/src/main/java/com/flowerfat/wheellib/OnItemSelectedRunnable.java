package com.flowerfat.wheellib;

/**
 * Created by janus on 2017/7/6.
 * OnItemSelectedRunnable
 */

final class OnItemSelectedRunnable implements Runnable {
    final LoopView loopView;

    OnItemSelectedRunnable(LoopView loopview) {
        this.loopView = loopview;
    }

    @Override
    public final void run() {
        loopView.onItemSelectedListener.onItemSelected(loopView.getSelectedItem());
    }
}
