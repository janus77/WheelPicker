package com.flowerfat.wheellib;

import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by janus on 2017/7/6.
 * LoopViewGestureListener
 */

final class LoopViewGestureListener extends GestureDetector.SimpleOnGestureListener {
    final LoopView loopView;

    LoopViewGestureListener(LoopView loopview) {
        this.loopView = loopview;
    }

    @Override
    public final boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        loopView.scrollBy(velocityY);
        return true;
    }
}
