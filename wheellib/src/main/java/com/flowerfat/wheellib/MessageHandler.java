package com.flowerfat.wheellib;

import android.os.Handler;
import android.os.Message;

/**
 * Created by janus on 2017/7/6.
 * MessageHandler
 */

final class MessageHandler extends Handler {
    public static final int WHAT_INVALIDATE_LOOP_VIEW = 1000;
    public static final int WHAT_SMOOTH_SCROLL = 2000;
    public static final int WHAT_ITEM_SELECTED = 3000;
    final LoopView loopview;

    MessageHandler(LoopView loopview) {
        this.loopview = loopview;
    }

    @Override
    public final void handleMessage(Message msg) {
        switch (msg.what) {
            case WHAT_INVALIDATE_LOOP_VIEW:
                this.loopview.invalidate();
                break;
            case WHAT_SMOOTH_SCROLL:
                this.loopview.smoothScroll(LoopView.ACTION.FLING);
                break;
            case WHAT_ITEM_SELECTED:
                this.loopview.onItemSelected();
        }

    }
}
