package com.flowerfat.wheellib;

/**
 * Created by janus on 2017/7/6.
 * OnItemSelectedListener
 */

public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
